package com.qifun.csvParser

import java.io.File
import org.junit.Assert._
import org.junit.Test

class CsvReaderTest {

  @Test def testParser: Unit = {
    val csvReader = CsvReader.open(new File("scala-csv-parser/src/test/test.csv"))
    val iterator = csvReader.iterator
    val line1 = Array("a", "b", "c,c", "d")
    val line2 = Array("1", "2", "3\n3", "4")

    val parsedLine1 = iterator.next()
    val parsedLine2 = iterator.next()

    for (i <- 0 until parsedLine1.length) {
      assertEquals(line1(i), parsedLine1(i))
      assertEquals(line2(i), parsedLine2(i))
    }
    csvReader.close();
  }
}