package com.qifun.csvParserUtil

import java.lang.Double

object Implicit {
  
  implicit def stringToInt(str: String):Int = Integer.parseInt(str)
  
  implicit def stringToString(str: String):String = str
  
  implicit def stringToBoolean(str: String):Boolean = str != 0 
  
  implicit def stringToDouble(str: String):Double = Double.parseDouble(str)
  
  implicit def stringToChar(str: String):Char = str(0)

}