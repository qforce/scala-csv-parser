package com.qifun.csvParser

import java.io.Reader
import java.io.Closeable
import scala.annotation.tailrec
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.io.InputStream

class CsvReader protected (private val reader: Reader) extends Closeable with Iterable[Array[String]] {
  private val format = new CsvFormat {}
  private val delimiter: Char = format.DELIMITER
  private val quoteChar: Char = format.QUOTECHAR

  private val parser: CsvParser = new CsvParser(format)
  private val lineReader: LineReader = new LineReader(reader)

  private def readNext(): Option[Array[String]] = {
    @tailrec def parseNext(lineReader: LineReader, leftOver: Option[String] = None): Option[Array[String]] = {
      val nextLine = lineReader.readLine
      nextLine match {
        case None => {
          if (leftOver.isDefined) {
            throw new Exception
          } else {
            None
          }
        }
        case Some(nextLine: String) => {
          val line = leftOver match {
            case None => nextLine
            case Some(string: String) => string + nextLine
          }
          parser.parseLine(line) match {
            case None => {
              parseNext(lineReader, Some(line))
            }
            case result => result
          }
        }
      }
    }
    parseNext(lineReader);
  }

  /**
   * 获得迭代器的方法
   */
  def iterator: Iterator[Array[String]] = new Iterator[Array[String]] {
    private var _next: Option[Array[String]] = None

    def hasNext: Boolean = {
      _next match {
        case None => {
          _next = readNext;
          _next.isDefined
        }
        case Some(row) => true
      }
    }

    def next(): Array[String] = {
      _next match {
        case Some(row) => {
          var t_row = row
          _next = None
          t_row
        }
        case None => readNext.getOrElse(throw new Exception)
      }
    }
  }

  /**
   * 关闭内部打开的各种流
   * 不会关闭外部定义各种流和reader
   */
  def close(): Unit = {
    lineReader.close()
    reader.close()
  }
}

object CsvReader {
  /**
   * 打开一个csv文件
   * @param file 要打开的文件类
   * @param encoding csv文件的编码方式，默认为UTF-8
   * @return 返回一个带迭代器的Reader，迭代器指向的内容为每行Csv的数据数组。
   */
  def open(file: File): CsvReader = {
    val fileInputStream = new FileInputStream(file)
    try {
      val reader = new InputStreamReader(fileInputStream, "UTF-8")
      new CsvReader(reader)
    } catch {
      case e: Throwable => fileInputStream.close(); throw e
    }
  }
  
  def open(inputStream: InputStream): CsvReader = {
    try {
      val reader = new InputStreamReader(inputStream, "UTF-8")
      new CsvReader(reader)
    } catch {
      case e: Throwable => inputStream.close(); throw e
    }
  }
}