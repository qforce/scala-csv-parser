package com.qifun.csvParser

import java.io.BufferedReader
import java.io.IOException
import java.io.Reader
import scala.collection.mutable.StringBuilder
import scala.annotation.tailrec

private[csvParser] class LineReader(reader: Reader) {
  private val bufferReader: BufferedReader = new BufferedReader(reader)

  def readLine(): Option[String] = {
    val stringBuilder: StringBuilder = StringBuilder.newBuilder
    @tailrec def read(): Unit = {
      val current: Int = bufferReader.read()
      if (current == 65279) { //Windows Utf8文件前缀
        read()
      } else if (current != -1) {
        stringBuilder.append(current.toChar)
        if (current != '\n' && current != '\u2028' && current != '\u2029' && current != '\u0085') {
          if (current == '\r') {
            bufferReader.mark(1)
            val next: Int = bufferReader.read()
            if (next == '\n') {
              stringBuilder.append('\n')
            } else {
              bufferReader.reset()
            }
          } else {
            read()
          }
        }
      }
    }
    read()
    if (stringBuilder.length == 0)
      None
    else
      Some(stringBuilder.result)
  }
  
  def close(): Unit = {
    bufferReader.close()
  }
}