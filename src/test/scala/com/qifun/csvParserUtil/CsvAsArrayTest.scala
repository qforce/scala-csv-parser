package com.qifun.csvParserUtil

import java.io.File
import org.junit.Assert._
import org.junit.Test
import com.qifun.csvParser.CsvReader

class CsvAsArrayTest {

  @Test def testGetLine: Unit = {
    val csvReader = CsvReader.open(new File("scala-csv-parser/src/test/test.csv"))
    val csvArray = new CsvAsArray(csvReader)
    val line1 = Array("a", "b", "c,c", "d")
    val line2 = Array("1", "2", "3\n3", "4")

    val parsedLine1 = csvArray.getValue(0)
    val parsedLine2 = csvArray.getValue(1)

    for (i <- 0 until parsedLine1.length) {
      assertEquals(line1(i), parsedLine1(i))
      assertEquals(line2(i), parsedLine2(i))
    }
    csvReader.close();
  }
  @Test def testGetValue: Unit = {
    val csvReader = CsvReader.open(new File("scala-csv-parser/src/test/test.csv"))
    val csvArray = new CsvAsArray(csvReader)
    val value00 = "a"
    val value02 = "c,c"
    val value12 = "3\n3"

    assertEquals(value00, csvArray.getValue(0, 0))
    assertEquals(value02, csvArray.getValue(0, 2))
    assertEquals(value12, csvArray.getValue(1, 2))
    csvReader.close();
  }
  @Test def testOutRange: Unit = {
    val csvReader = CsvReader.open(new File("scala-csv-parser/src/test/test.csv"))
    val csvArray = new CsvAsArray(csvReader)

    try {
      csvArray.getValue(10)
    } catch {
      case exception: Exception => assertEquals("out of range!", exception.getMessage())
    }
    try {
      csvArray.getValue(0, 10)
    } catch {
      case exception: Exception => assertEquals("out of range!", exception.getMessage())
    }
    try {
      csvArray.getValue(10, 0)
    } catch {
      case exception: Exception => assertEquals("out of range!", exception.getMessage())
    }
    try {
      csvArray.getValue(10, 10)
    } catch {
      case exception: Exception => assertEquals("out of range!", exception.getMessage())
    }
    csvReader.close();

  }
}