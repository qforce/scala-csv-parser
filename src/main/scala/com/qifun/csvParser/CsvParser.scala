package com.qifun.csvParser


import scala.collection.mutable.ArrayBuilder
import scala.collection.mutable.StringBuilder
import scala.annotation.tailrec

private[csvParser] object CsvParser {
  abstract sealed trait State
  case object Start extends State
  case object Delimiter extends State
  case object Field extends State
  case object End extends State
  case object QuoteStart extends State
  case object QuoteEnd extends State
  case object QuotedField extends State

  def parser(input: String, escapedChar: Char, delimiter: Char, quoteChar: Char): Option[Array[String]] = {
    val buffer: Array[Char] = input.toCharArray()
    val fields: ArrayBuilder[String] = Array.newBuilder[String]
    val field: StringBuilder = StringBuilder.newBuilder

    //用来标识当前读取的状态
    var state: State = Start

    @tailrec def traverseBuffer(pos: Int) {
      if (pos < buffer.length && state != End) {
        val current = buffer(pos)
        state match {
          case Start | Delimiter => {
            current match {
              case `quoteChar` => {
                state = QuoteStart
                traverseBuffer(pos + 1)
              }
              case `delimiter` => {
                fields += field.result
                field.clear
                state = Delimiter
                traverseBuffer(pos + 1)
              }
              case '\n' | '\u2028' | '\u2029' | '\u0085' => { //换行
                fields += field.result
                field.clear
                state = End
                traverseBuffer(pos + 1)
              }
              case '\r' => {
                if (pos + 1 < buffer.length && buffer(1) == '\n') {
                  traverseBuffer(pos + 1)
                } else {
                  fields += field.result
                  field.clear
                  state = End
                  traverseBuffer(pos + 1)
                }
              }
              case x => {
                field += x
                state = Field
                traverseBuffer(pos + 1)
              }
            }
          }
          case Field => {
            current match {
              case `escapedChar` => {
                if (pos + 1 < buffer.length) {
                  if (buffer(pos + 1) == escapedChar || buffer(pos + 1) == delimiter) {
                    field += buffer(pos + 1)
                    state = Field
                    traverseBuffer(pos + 2)
                  } else {
                    throw new Exception(buffer.mkString)
                  }
                } else {
                  state = QuoteEnd
                  traverseBuffer(pos + 1)
                }
              }
              case `delimiter` => {
                fields += field.result
                field.clear
                state = Delimiter
                traverseBuffer(pos + 1)
              }
              case '\n' | '\u2028' | '\u2029' | '\u0085' => { //换行
                fields += field.result
                field.clear
                state = End
                traverseBuffer(pos + 1)
              }
              case '\r' => {
                if (pos + 1 < buffer.length && buffer(1) == '\n') {
                  traverseBuffer(pos + 1)
                } else {
                  fields += field.result
                  field.clear
                  state = End
                  traverseBuffer(pos + 1)
                }
              }
              case x => {
                field += x
                state = Field
                traverseBuffer(pos + 1)
              }
            }
          }
          case QuoteStart => {
            current match {
              case `quoteChar` => {
                if (pos + 1 < buffer.length && buffer(pos + 1) == quoteChar) {
                  field += quoteChar
                  state = QuotedField
                  traverseBuffer(pos + 2)
                } else {
                  state = QuoteEnd
                  traverseBuffer(pos + 1)
                }
              }
              case x => {
                field += x;
                state = QuotedField
                traverseBuffer(pos + 1)
              }
            }
          }
          case QuoteEnd => {
            current match {
              case `delimiter` => {
                fields += field.result
                field.clear
                state = Delimiter
                traverseBuffer(pos + 1)
              }
              case '\n' | '\u2028' | '\u2029' | '\u0085' => { //换行
                fields += field.result
                field.clear
                state = End
                traverseBuffer(pos + 1)
              }
              case '\r' => {
                if (pos + 1 < buffer.length && buffer(1) == '\n') {
                  traverseBuffer(pos + 1)
                } else {
                  fields += field.result
                  field.clear
                  state = End
                  traverseBuffer(pos + 1)
                }
              }
              case _ => {
                throw new Exception(buffer.mkString)
              }
            }
          }
          case QuotedField => {
            current match {
              case `escapedChar` if escapedChar != quoteChar => {
                if (pos + 1 < buffer.length) {
                  if (buffer(pos + 1) == escapedChar || buffer(pos + 1) == quoteChar) {
                    field += buffer(pos + 1)
                    state = QuotedField
                    traverseBuffer(pos + 2)
                  } else {
                    throw new Exception(buffer.mkString)
                  }
                } else {
                  throw new Exception(buffer.mkString)
                }
              }
              case `quoteChar` => {
                if (pos + 1 < buffer.length &&buffer(pos + 1) == quoteChar) {
                  field += quoteChar
                  state = QuotedField
                  traverseBuffer(pos + 2)
                } else {
                  state = QuoteEnd
                  traverseBuffer(pos + 1)
                }
              }
              case x => {
                field += x
                state = QuotedField
                traverseBuffer(pos + 1)
              }
            }
          }
          case End => {
        	sys.error("unexpected error")
          }
        }
      }
    }

    traverseBuffer(0)
    
    state match {
      case Delimiter => {
        fields += ""
        Some(fields.result)
      }
      case QuotedField => {
        None
      }
      case _ => {
        if (!field.isEmpty) {
          //文件末尾没有crlf
          state match {
            case Field | QuoteEnd => {
              fields += field.result
            }
            case _ => 
          }
        }
        Some(fields.result)
      }
    }
  }
}

private[csvParser] class CsvParser(format:CsvFormat) {
  def parseLine(input: String): Option[Array[String]] = CsvParser.parser(input, format.ESCAPECHAR, format.DELIMITER, format.QUOTECHAR)
}