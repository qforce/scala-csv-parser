package com.qifun.csvParserUtil

import java.io.File
import org.junit.Assert._
import org.junit.Test
import com.qifun.csvParser.CsvReader

class CsvAsMapTest {

  @Test def testApply: Unit = {
    val csvReader = CsvReader.open(new File("scala-csv-parser/src/test/武将配置表.csv"))
    val csvMap = new CsvAsMap(csvReader)
    val value11 = "张飞"
    val value14 = "13"
    val value27 = "200字"
    val value225 = ""
    val wrong = "can't find!"

    assertEquals(value11, csvMap("1", "武将名称"))
    assertEquals(value14, csvMap("1", "武将资质"))
    assertEquals(value27, csvMap.apply("2", "简介"))
    assertEquals(value225, csvMap.apply("2", "技能1参数2"))
    try {
      csvMap("2", "技能1参数21")
    } catch {
      case exception: Exception => assertEquals(wrong, exception.getMessage())
    }
    csvReader.close();
  }
  @Test def testGet: Unit = {
    val csvReader = CsvReader.open(new File("scala-csv-parser/src/test/武将配置表.csv"))
    val csvMap = new CsvAsMap(csvReader)
    val value11 = Some("张飞")
    val value14 = Some("13")
    val value27 = Some("200字")
    val value225 = Some("")
    val none: Option[String] = None

    assertEquals(value11, csvMap.get("1", "武将名称"))
    assertEquals(value14, csvMap.get("1", "武将资质"))
    assertEquals(value27, csvMap.get("2", "简介"))
    assertEquals(none, csvMap.get("刘备", "技能1参数2"))
    assertEquals(none, csvMap.get("张飞", "性别"))
    csvReader.close();
  }
  @Test def testGetOrElse: Unit = {
    val csvReader = CsvReader.open(new File("scala-csv-parser/src/test/武将配置表.csv"))
    val csvMap = new CsvAsMap(csvReader)
    val value11 = "张飞"
    val value14 = "13"
    val value27 = "200字"
    val value225 = ""
    val default = "hahaha...."

    assertEquals(value11, csvMap.getOrElse("1", "武将名称", default))
    assertEquals(value14, csvMap.getOrElse("1", "武将资质", default))
    assertEquals(value27, csvMap.getOrElse("2", "简介", default))
    assertEquals(value225, csvMap.getOrElse("2", "技能1参数2", default))
    assertEquals(default, csvMap.getOrElse("张飞", "性别", default))
    csvReader.close();
  }
}