package com.qifun.csvParser

private[csvParser] trait CsvFormat {
  
  val DELIMITER: Char = ','

  val QUOTECHAR: Char = '"'

  val ESCAPECHAR: Char = '"'

}