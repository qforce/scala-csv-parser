package com.qifun.csvParserUtil

import com.qifun.csvParser.CsvReader
import java.io.File
import scala.annotation.tailrec
import Implicit._

/*
 * 实现将CsvAsArray的数据结构转化成scala的二维数组
 * 封装了两个函数
 * getValue(a),得到第a行的数据
 * getValue(a,b),得到第a行b列的数据
 */
class CsvAsArray(private val csvReader: Iterable[Array[String]]) extends Iterable[Array[String]] {
  private val csvArray = searchCsvArray()
  private val highLength = csvArray.length
  private val wideLength = if (highLength > 0) csvArray(0).length else 0
  /**
   * 读取csv表中第a行第b列的内容（a,b从0开始）
   * @param a 读取第a行
   * @param b 读取第b列
   * @return csv表中第a行第b列的内容
   * @throws Exception 抛出内容为"out of range!"的异常
   */
 def getValue[B](a: Int, b: Int)(implicit f:String => B): B = {
    if ((a < highLength) && (b < wideLength) && (a >= 0) && (b >= 0)) f(csvArray(a)(b))
    else throw new Exception("out of range!")
  }
  /**
   * 读取csv表中第a行的内容（a从0开始）
   * @param a 读取第a行
   * @return csv表中第a行第b列的内容
   */
  def getValue(a: Int): Array[String] = {
    if ((a < highLength) && (a >=0 )) csvArray(a)
    else throw new Exception("out of range!")
  }
  
  def iterator: Iterator[Array[String]] = csvArray.iterator
  
  private def searchCsvArray(): Array[Array[String]] = {
    val iterator = csvReader.iterator                  
    val csvArray = Array.newBuilder[Array[String]]		
    @tailrec def getCsvLine() {
      if (iterator.hasNext) {
        csvArray += iterator.next()
        getCsvLine()
      }
    }

    getCsvLine()
    csvArray.result()
  }
}