package com.qifun.csvParserUtil

import com.qifun.csvParser.CsvReader
import java.io.File
import scala.annotation.tailrec
import scala.collection.immutable.Map
import Implicit._

class CsvAsMap(csvReader: Iterable[Array[String]]) {  
  private val csvMap = searchCsvMap()
  val length = csvMap.size
  val iterator = csvMap.iterator
  /**
   * 读取csv中的内容
   * @param row 行的标号为row
   * @param list 列的标号为list
   * @return 行的标号为row，列的标号为list的内容。
   * @throws Exception 抛出内容为"can't find!"的异常
   */
  def apply[B](row: String, list: String)(implicit f: String => B): B = {
    try{
      f(csvMap(row)(list))
    }catch{
      case _: Throwable =>throw new Exception("can't find!")
    }
  }
  /**
   * 读取csv中的内容
   * @param row 行的标号为row
   * @param list 列的标号为list
   * @return 行的标号为row，列的标号为list的内容。如果查找不到，返回None
   */
  def get[B](row: String, list: String)(implicit f: String => B): Option[B] = {
    csvMap.get(row) match
    {
      case Some(x:Map[String, String]) => x.get(list) match{
        case Some(y:String) => Some(f(y))
        case None => None
      }
      case None => None
    }
  }
  /**
   * 读取csv中的内容
   * @param row 行的标号为row
   * @param list 列的标号为list
   * @param default 无法找到返回的内容
   * @return 行的标号为row，列的标号为list的内容。如果查找不到，返回default
   */
  def getOrElse[B](row: String, list: String, default: String)(implicit f: String => B): B = {
    try{
      f(csvMap(row)(list))
    }catch{
      case _: Throwable =>default
    }
  }

  private def searchCsvMap(): Map[String, Map[String, String]] = {
    val iterator = csvReader.iterator
    val listName = iterator.next()
    val csvMap = Map.newBuilder[String, Map[String, String]]
    @tailrec def getCsvMap() {
      if (iterator.hasNext) {
        val line = iterator.next()
        csvMap += (line(0) -> getCsvLine(line))
        getCsvMap()
      }
    }
    def getCsvLine(line: Array[String]): Map[String, String] = {
      val lineMap = Map.newBuilder[String, String]
      @tailrec def getCsvValue(i: Int) {
        if (i < line.length) {
          lineMap += (listName(i) -> line(i))
          getCsvValue(i + 1)
        }
      }
      getCsvValue(0)
      lineMap.result()
    }
    getCsvMap()
    csvMap.result()
  }
}